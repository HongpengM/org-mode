#+INCLUDE: theme/style.org
#+TITLE: Linux Kernel
#+DESCRIPTION:
#+KEYWORDS:
#+STARTUP:  content
#+DATE: 2018-12-20

- [[wiki:index][Index]]

- Related:

[[wiki:Operating Systems][Operating Systems]]

* Linux Kernel

** Introduction

*** The relationship between applications, the kernel and hardware

#+CAPTION: Relationship amonng applications, the kernel and hardware
[[file:Linux Kernel/relationship_of_kernel_user_hardware.png]]

*** Linux Kernel Versions

#+CAPTION: Linux kernel versions
[[file:Linux Kernel/linux_kernel_versions.png]]

** Get started from the kernel

*** Get kernel source

Use git to clone it from the source. [[https://www.kernel.org]]

*** The kernel source Tree

| Directory     | Description                                   |
|---------------+-----------------------------------------------|
| arch          | Architecture-specific source                  |
| block         | Block I/O layer                               |
| crypto        | Crypto API                                    |
| Documentation | Kernel source documentation                   |
| drivers       | Device drivers                                |
| firmware      | Device firmware needed to use certain drivers |
| fs            | the VFS and individual filesystems            |
| include       | Kernel headers                                |
| init          | Kernel boot and initialization                |
| ipc           | Interprocess communication code               |
| kernel        | Core subsystems, such as the scheduler        |
| lib           | Helper routines                               |
| mm            | memory management subsystem and the VM        |
| net           | networking subsystem                          |
| samples       | Sample, demonstrative code                    |
| scripts       | Scripts used to build the kernel              |
| security      | Linux Security Module                         |
| sound         | sound subsystem                               |
| usr           | Early uesr-space code (called initramfs)      |
| tools         | Tools helpful for developing linux            |
| virt          | Virtualization infrastructure                 |

*** Configuring the Kernel

#+BEGIN_SRC bash
# plain configure: text based, command-line
make config
# config the menu part
make menuconfig
# config gtk+ based graphical utility
make gconfig
# config the defaults for architecture
make defconfig
# update config
make oldconfig

make
#+END_SRC

**** Minimizing build noise
#+BEGIN_SRC bash
make > ../detritus
# Or 
make > /dev/null
#+END_SRC


*** The feaures of kernel developing
- The kernel has access to neither the C library nor the standard C headers.\
  Only exists GNU C
- The kernel is coded in GNU C.
- The kernel lacks the memory protection afforded to user-space.
- The kernel cannot easily execute floating-point operations.
- The kernel has a small per-process fixed-size stack.
- Because the kernel has asynchronous interrupts, is preemptive, and supports SMP, synchronization and concurrency are major concerns within the kernel.
- Portability is important.



** REPORT Process Management

   - State "REPORT"     from              [2018-12-20 Thu 15:16] \\
     Marked
