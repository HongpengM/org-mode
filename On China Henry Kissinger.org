#+INCLUDE: theme/style.org
#+TITLE: On China Henry Kissinger
#+DESCRIPTION:
#+KEYWORDS:
#+STARTUP:  content
#+DATE: 2018-12-16

- [[wiki:index][Index]]

- Related:

* TODO On China Henry Kissinger                                                                        :books:
SCHEDULED: <2018-12-17 Mon>



#+CAPTION: China's boarder 1
[[file:On%20China%20Henry%20Kissinger/China's%20boarder%201_2018-12-16_15-38-59.png]]

#+CAPTION: China's boarder 2
[[file:On%20China%20Henry%20Kissinger/China's%20boarder%202_2018-12-16_15-45-16.png]]


** DONE Prologue
   CLOSED: [2018-12-16 Sun 17:28]
   - CLOSING NOTE [2018-12-16 Sun 17:28] \\
     Finished the prologue part.
*** One and a half war with India

#+BEGIN_QUOTE
China and India, Mao told his commanders, had previously fought “one and a half” wars. Beijing could draw operational lessons from each. The first war had occurred over 1,300 years earlier, during the Tang Dynasty (618–907), when China dispatched troops to support an Indian kingdom against an illegitimate and aggressive rival. After China’s intervention, the two countries had enjoyed centuries of flourishing religious and economic exchange. The lesson learned from the ancient campaign, as Mao described it, was that China and India were not doomed to perpetual enmity. They could enjoy a long period of peace again, but to do so, China had to use force to “knock” India back “to the negotiating table.” The “half war,” in Mao’s mind, had taken place seven hundred years later, when the Mongol ruler Timurlane sacked Delhi. (Mao reasoned that since Mongolia and China were then part of the same political entity, this was a “half” Sino-Indian war.) Timurlane had won a significant victory, but once in India his army had killed over 100,000 prisoners. This time, Mao enjoined his Chinese forces to be “restrained and principled.”[fn:1]
#+END_QUOTE


*** On the retreat from India

#+BEGIN_QUOTE
In no other country is it conceivable that a modern leader would initiate a major national undertaking by invoking strategic principles from a millennium-old event—nor that he could confidently expect his colleagues to understand the significance of his allusions. Yet China is singular. No other country can claim so long a continuous civilization, or such an intimate link to its ancient past and classical principles of strategy and statesmanship.
#+END_QUOTE


***  Legitimacy 合法性

#+BEGIN_QUOTE
Other societies, the United States included, have claimed universal applicability for their values and institutions. Still, none equals China in persisting—and persuading its neighbors to acquiesce—in such an elevated conception of its world role for so long, and in the face of so many historical vicissitudes. From the emergence of China as a unified state in the third century B.C. until the collapse of the Qing Dynasty in 1912, China stood at the center of an East Asian international system of remarkable durability. The Chinese Emperor was conceived of (and recognized by most neighboring states) as the pinnacle of a universal political hierarchy, with all other states’ rulers theoretically serving as vassals. Chinese language, culture, and political institutions were the hallmarks of civilization, such that even regional rivals and foreign conquerors adopted them to varying degrees as a sign of their own legitimacy (often as a first step to being subsumed within China).
#+END_QUOTE


#+BEGIN_QUOTE
其他社会，包括美国，都声称自己的价值观和体制普世适用。然而，唯有中国在历史长河的变迁中始终视自己为世界的中心，并使四周邻国默认这一观点。从公元前3世纪中国崛起为一个统一的强国到1911年清王朝的覆亡，中国一直占据着历史悠久的东亚国际体系的中心地位。中国的皇帝高踞一个涵盖宇内的政治等级体制的顶峰（而且得到大多数邻国的认可），其他国家的君主在理论上都是他的诸侯。中国的语言、文化和政治体制是文明的标志。哪怕是地区性的竞争对手和外来征服者也分别在不同程度上吸收了中华文明，作为自己合法性的标志（常常是走向被中国同化的第一步）。
#+END_QUOTE

*** Word view of Chinese

#+BEGIN_QUOTE
The traditional cosmology endured despite catastrophes and centuries-long periods of political decay. Even when China was weak or divided, its centrality remained the touchstone of regional legitimacy; aspirants, both Chinese and foreign, vied to unify or conquer it, then ruled from the Chinese capital without challenging the basic premise that it was the center of the universe. While other countries were named after ethnic groups or geographical landmarks, China called itself zhongguo—the “Middle Kingdom” or the “Central Country.” 2 Any attempt to understand China’s twentieth-century diplomacy or its twenty-first-century world role must begin—even at the cost of some potential oversimplification—with a basic appreciation of the traditional context.
#+END_QUOTE
=宇宙观=
#+BEGIN_QUOTE
虽然中国历经劫难，有时政治衰微长达数百年之久，但中国传统的宇宙观始终没有泯灭。即使在贫弱分裂时期，它的中心地位仍然是检验地区合法性的试金石。中外枭雄竞相逐鹿中国，一俟统一或征服它后，即从中国的首都号令天下，对中国乃宇内之中心这一前提从未有过任何异议。
#+END_QUOTE


** TODO The Singularity of China中国的独特性


*** Societies and nations 关于起源

=The Chinese see themselves eternal=

Kissinger On Confucious
#+BEGIN_QUOTE
This paradox of Chinese history recurs with the ancient sage Confucius: again, he is seen as the “founder” of a culture although he stressed that he had invented nothing, that he was merely trying to reinvigorate the principles of harmony which had once existed in the golden age but had been lost in Confucius’s own era of political chaos.
#+END_QUOTE

the nineteenth-century missionary and traveler, the Abbé Régis-Evariste Huc observed:
#+BEGIN_QUOTE
Chinese civilization originates in an antiquity so remote that we vainly endeavor to discover its commencement. There are no traces of the state of infancy among this people. This is a very peculiar fact respecting China. We are accustomed in the history of nations to find some well-defined point of departure, and the historic documents, traditions, and monuments that remain to us generally permit us to follow, almost step by step, the progress of civilization, to be present at its birth, to watch its development, its onward march, and in many cases, its subsequent decay and fall. But it is not thus with the Chinese. They seem to have been always living in the same stage of advancement as in the present day; and the data of antiquity are such as to confirm that opinion.[fn:2]
#+END_QUOTE

The language system
#+BEGIN_QUOTE
When Chinese written characters first evolved, during the Shang Dynasty in the second millennium B.C., ancient Egypt was at the height of its glory. The great city-states of classical Greece had not yet emerged, and Rome was millennia away. Yet the direct descendant of the Shang writing system is still used by well over a billion people today. 
#+END_QUOTE

The strong reunite inclination of China
#+BEGIN_QUOTE
At the same time, Chinese history featured many periods of civil war, interregnum, and chaos. After each collapse, the Chinese state reconstituted itself as if by some immutable law of nature. At each stage, a new uniting figure emerged, following essentially the precedent of the Yellow Emperor, to subdue his rivals and reunify China (and sometimes enlarge its bounds). The famous opening of The Romance of the Three Kingdoms, a fourteenth-century epic novel treasured by centuries of Chinese (including Mao, who is said to have pored over it almost obsessively in his youth), evokes this continuous rhythm: “The empire, long divided, must unite; long united, must divide. Thus it has ever been.”[fn:3]Each period of disunity was viewed as an aberration. Each new dynasty reached back to the previous dynasty’s principles of governance in order to reestablish continuity. The fundamental precepts of Chinese culture endured, tested by the strain of periodic calamity.
#+END_QUOTE
#+BEGIN_QUOTE
After 221 B.C., China maintained the ideal of empire and unity but followed the practice of fracturing, then reuniting, in cycles sometimes lasting several hundred years.
When the state fractured, wars between the various components were fought savagely.
#+END_QUOTE



*** The era of Chinese Preeminence

=Though China has unparallel technologies in the middle ages, she stay closed=
#+BEGIN_QUOTE
Their exclusion of foreigners and confinement to their own country has, by depriving them of all opportunities of making comparisons, sadly circumscribed their ideas; they are thus totally unable to free themselves from the dominion of association, and judge everything by rules of purely Chinese convention.
#+END_QUOTE

The illusion of central country
#+BEGIN_QUOTE
As late as 1863, China’s Emperor (himself a member of a “foreign” Manchu Dynasty that had conquered China two centuries earlier) dispatched a letter informing Abraham Lincoln of China’s commitment to good relations with the United States. The Emperor based his communication on the grandiloquent assurance that, “[h]aving, with reverence, received the commission from Heaven to rule the universe, we regard both the middle empire [China] and the outside countries as constituting one family, without any distinction."[fn:4]

#+END_QUOTE

#+BEGIN_QUOTE
迟至1863年，中国的皇帝（他本人即是200年前征服了中国的“异族”——满族王族的一员）致函林肯，告之中国致力于保持与美国的友好关系。他在信函中自高自大地称：“朕承天命，抚有四海，视中国和异邦同为一家，彼此无异也。”
#+END_QUOTE

=The Richness and Strongness of China=
#+BEGIN_QUOTE
until the Industrial Revolution, China was far richer. United by a vast system of canals connecting the great rivers and population centers, China was for centuries the world’s most productive economy and most populous trading area.

As late as 1820, it produced over 30 percent of world GDP—an amount exceeding the GDP of Western
Europe, Eastern Europe, and the United States combined.[fn:5]
#+END_QUOTE

Writing in 1736, the French Jesuit Jean-Baptiste Du Halde summed up the awestruck reactions of Western visitors to China:
#+BEGIN_QUOTE
The riches peculiar to each province, and the facility of conveying merchandise, by means of rivers and canals, have rendered the domestic trade of the empire always very flourishing. . . . The inland trade of China is so great that the commerce of all Europe is not to be compared therewith; the provinces being like so many kingdoms, which communicate to each other their respective productions.[fn:6]
#+END_QUOTE

Thirty years later, the French political economist François Quesnay went even further:
#+BEGIN_QUOTE
[N]o one can deny that this state is the most beautiful in the world, the most densely populated, and the most flourishing kingdom known. Such an empire as that of China is equal to what all Europe would be if the latter were united under a single sovereign.[fn:7]
#+END_QUOTE

The pride of China
#+BEGIN_QUOTE
Trade with China was so prized that it was with only partial exaggeration that Chinese elites described it not as ordinary economic exchange but as “tribute” to China’s superiority.
#+END_QUOTE

*** Confucianism



[fn:1]: John W. Garver, “China’s Decision for War with India in 1962,”
[fn:2]: Abbé Régis-Evariste Huc, The Chinese Empire (London: Longman, Brown, Green & Longmans, 1855), as excerpted in Franz Schurmann and Orville Schell, eds., Imperial China: The Decline of the Last Dynasty and the Origins of Modern China—The 18th and 19th Centuries (New York: Vintage, 1967), 31.
[fn:3]: Luo Guanzhong, The Romance of the Three Kingdoms, trans. Moss Roberts (Beijing: Foreign Languages Press, 1995), 1.
[fn:4]: Anticipating that his colleagues in Washington would object to this proclamation of Chinese universal jurisdiction, the American envoy in Beijing obtained an alternate translation and textual exegesis from a local British expert. The latter explained that the offending expression—literally “to soothe and bridle the world”—was a standard formulation, and that the letter to Lincoln was in fact a (by the Chinese court’s standards) particularly modest document whose phrasing indicated genuine goodwill. Papers Relating to Foreign Affairs Accompanying the Annual Message of the President to the First Session of the Thirty-eighth Congress, vol. 2 (Washington, D.C.: U.S. Government Printing Office, 1864), Document No. 33 (“Mr. Burlingame to Mr. Seward, Peking, January 29, 1863”), 846–48.
[fn:5]: Angus Maddison, The World Economy: A Millennial Perspective (Paris: Organisation for Economic Co-operation and Development, 2006), Appendix B, 261–63. It must be allowed that until the Industrial Revolution, total GDP was tied more closely to population size; thus China and India outstripped the West in part by virtue of their larger populations. I would like to thank Michael Cembalest for bringing these figures to my attention.
[fn:6]: Jean-Baptiste Du Halde, Description géographique, historique, chronologique, politique, et physique de l’empire de la Chine et de la Tartarie chinoise (La Haye: H. Scheurleer, 1736), as translated and excerpted in Schurmann and Schell, eds., Imperial China, 71.
[fn:7]: François Quesnay, Le despotisme de la Chine, as translated and excerpted in Schurmann and Schell, eds., Imperial China, 115.
